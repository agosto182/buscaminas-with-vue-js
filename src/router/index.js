import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import easy from '@/components/game/easy';
import medium from '@/components/game/medium';

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/',name: 'Home',component: home},
    {path: '/endgame/:msg',name: 'endgame',component: home},
    {path:'/game/easy',component:easy},
    {path:'/game/medium',component:medium}
  ]
})
